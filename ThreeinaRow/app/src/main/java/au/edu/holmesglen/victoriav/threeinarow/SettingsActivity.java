package au.edu.holmesglen.victoriav.threeinarow;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

/**
 * SettingsActivity Class
 * @author Victoria Vilarroel
 * Description: Displays the settings page and save the prefeences in a sharedPreferences
 * file in the device. also gets the preferences for the file
 */
public class SettingsActivity extends AppCompatActivity {

    public static final String COLOR_PREFERENCES = "ColorPreferences";
    public static final String TAG = "ColorPreferences";
    public static final String COLOR1 = "color1Key";
    public static final String COLOR2 = "color2Key";
    public static final String PINK = "pink";
    public static final String PURPLE = "purple";
    public static final String ORANGE = "orange";
    public static final String YELLOW = "yellow";
    public static final String GREEN = "green";
    public static final String BLUE = "blue";


    static SharedPreferences sharedPrefs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        sharedPrefs = getSharedPreferences(COLOR_PREFERENCES, Context.MODE_PRIVATE);

        RadioGroup group1 = findViewById(R.id.grpFirstColor);
        RadioGroup group2 = findViewById(R.id.grpSecondColor);

        getData(COLOR1);
        getData(COLOR2);

        FloatingActionButton btnBack = findViewById(R.id.btnBack2);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), MenuActivity.class);
                startActivity(i);
            }
        });

        group1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case -1:
                        Log.i(TAG, "Choices cleared!");
                        break;
                    case R.id.radPink:
                        Log.i(TAG, "Chose pink");
                        saveValue(PINK, COLOR1);
                        break;
                    case R.id.radPurple:
                        Log.i(TAG, "Chose purple");
                        saveValue(PURPLE, COLOR1);
                        break;
                    case R.id.radOrange:
                        Log.i(TAG, "Chose Orange");
                        saveValue(ORANGE, COLOR1);
                        break;
                    default:
                        Log.i(TAG, "Huh?");
                        break;
                }
            }
        });

        group2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case -1:
                        Log.i(TAG, "Choices cleared!");
                        break;
                    case R.id.radYellow:
                        Log.i(TAG, "Chose yellow");
                        saveValue(YELLOW, COLOR2);
                        break;
                    case R.id.radGreen:
                        Log.i(TAG, "Chose green");
                        saveValue(GREEN, COLOR2);
                        break;
                    case R.id.radBlue:
                        Log.i(TAG, "Chose blue");
                        saveValue(BLUE, COLOR2);
                        break;
                    default:
                        Log.i(TAG, "Huh?");
                        break;
                }
            }
        });

    }

    public void saveValue(String color1, String key) {
        String var = color1;
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putString(key, var);
        editor.commit();
    }

    public void getData(String key) {

        Item blue = new Item(R.drawable.blue, BLUE);
        Item green = new Item(R.drawable.green, GREEN);
        Item yellow = new Item(R.drawable.yellow, YELLOW);
        Item orange = new Item(R.drawable.orange, ORANGE);
        Item pink = new Item(R.drawable.pink, PINK);
        Item purple = new Item(R.drawable.purple, PURPLE);

        String val;
        RadioButton rad;
        if (sharedPrefs.contains(key)) {
            if (key.equals(COLOR1)) {
                val = sharedPrefs.getString(key, PINK);
                if (val.equals(PINK)) {
                    rad = findViewById(R.id.radPink);
                    GameActivity.COLOR1ITEM.setItem(pink);
                } else if (val.equals(PURPLE)) {
                    rad = findViewById(R.id.radPurple);
                    GameActivity.COLOR1ITEM.setItem(purple);
                } else {
                    rad = findViewById(R.id.radOrange);
                    GameActivity.COLOR1ITEM.setItem(orange);
                }

            } else {
                val = sharedPrefs.getString(key, YELLOW);
                if (val.equals(BLUE)) {
                    rad = findViewById(R.id.radBlue);
                    GameActivity.COLOR2ITEM.setItem(blue);
                } else if (val.equals(GREEN)) {
                    rad = findViewById(R.id.radGreen);
                    GameActivity.COLOR2ITEM.setItem(green);
                } else {
                    rad = findViewById(R.id.radYellow);
                    GameActivity.COLOR2ITEM.setItem(yellow);
                }
            }
            rad.setChecked(true);
        } else {
            rad = findViewById(R.id.radPink);
            GameActivity.COLOR1ITEM.setItem(pink);
            rad.setChecked(true);
            rad = findViewById(R.id.radYellow);
            GameActivity.COLOR2ITEM.setItem(yellow);
            rad.setChecked(true);
        }

    }
}
