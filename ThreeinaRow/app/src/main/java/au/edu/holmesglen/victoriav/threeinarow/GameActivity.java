package au.edu.holmesglen.victoriav.threeinarow;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;

/**
 * GameActivity Class
 * @author Victoria Vilarroel
 * Description: Displays the view for the game and starts the game
 */
public class GameActivity extends AppCompatActivity {

    SharedPreferences sharedPrefs;


    public static final Item COLOR1ITEM = new Item();
    public static final Item COLOR2ITEM = new Item();

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        sharedPrefs = getSharedPreferences(SettingsActivity.COLOR_PREFERENCES, Context.MODE_PRIVATE);

        getData(SettingsActivity.COLOR1);
        getData(SettingsActivity.COLOR2);

        FloatingActionButton btnBack = findViewById(R.id.btnBack);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(GameActivity.this);
                alert.setTitle("Back");
                alert.setMessage("Are you sure you want to go back?");
                alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent(getApplicationContext(), MenuActivity.class);
                        startActivity(i);
                    }
                });
                alert.setNegativeButton("No", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alert.show();
            }
        });

        int gridSize = 4;
        GridView grid = findViewById(R.id.gameGrid);
        ImageView hint = findViewById(R.id.imgNextColor);

        final Game game = new Game(gridSize, COLOR1ITEM, COLOR2ITEM, grid, hint, this);
        final AlertDialog.Builder alert = new AlertDialog.Builder(GameActivity.this);
        alert.setMessage("You want to play again?");
        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent i = new Intent(getApplicationContext(), GameActivity.class);
                startActivity(i);
            }
        });
        alert.setNegativeButton("No", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent i = new Intent(getApplicationContext(), MenuActivity.class);
                startActivity(i);
            }
        });
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                game.move(position, view);
                if (game.endOfMoves()) {
                    alert.setTitle("You Won!!");
                    alert.show();
                } else {
                    if (game.checkGame()) {
                        alert.setTitle("You Lose!!");
                        alert.show();
                    }
                }

            }
        });
    }

    /**
     *
     * @param key
     */
    public void getData(String key) {

        Item blue = new Item(R.drawable.blue, SettingsActivity.BLUE);
        Item green = new Item(R.drawable.green, SettingsActivity.GREEN);
        Item yellow = new Item(R.drawable.yellow, SettingsActivity.YELLOW);
        Item orange = new Item(R.drawable.orange, SettingsActivity.ORANGE);
        Item pink = new Item(R.drawable.pink, SettingsActivity.PINK);
        Item purple = new Item(R.drawable.purple, SettingsActivity.PURPLE);

        String val;
        if (sharedPrefs.contains(key)) {
            if (key.equals(SettingsActivity.COLOR1)) {
                val = sharedPrefs.getString(key, SettingsActivity.PINK);
                if (val.equals(SettingsActivity.PINK)) {
                    COLOR1ITEM.setItem(pink);
                } else if (val.equals(SettingsActivity.PURPLE)) {
                    COLOR1ITEM.setItem(purple);
                } else {
                    COLOR1ITEM.setItem(orange);
                }

            } else {
                val = sharedPrefs.getString(key, SettingsActivity.YELLOW);
                if (val.equals(SettingsActivity.BLUE)) {
                    COLOR2ITEM.setItem(blue);
                } else if (val.equals(SettingsActivity.GREEN)) {
                    COLOR2ITEM.setItem(green);
                } else {
                    COLOR2ITEM.setItem(yellow);
                }
            }
        } else {
            COLOR1ITEM.setItem(pink);
            COLOR2ITEM.setItem(yellow);
        }

    }

}
