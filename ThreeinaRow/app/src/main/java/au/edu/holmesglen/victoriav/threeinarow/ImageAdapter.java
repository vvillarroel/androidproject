package au.edu.holmesglen.victoriav.threeinarow;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

/**
 * ImageAdapter Class
 * @author Victoria
 * Description: Allows to add an array of items to a grid view automatically.
 */
public class ImageAdapter extends BaseAdapter {
    Item[] gridArray;
    int layoutResourseId;
    private Context mContext;

    public ImageAdapter(Context c, Item[] gridArray) {
        mContext = c;
        this.gridArray = gridArray;
    }

    public int getCount() {
        return gridArray.length;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {
            imageView = new ImageView(mContext);

            int width = ((GridView) parent).getColumnWidth();

            imageView.setLayoutParams(new GridView.LayoutParams(width, width));
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            imageView.setPadding(0, 0, 0, 0);
        } else {
            imageView = (ImageView) convertView;
        }
        imageView.setImageResource(gridArray[position].getColor());
        return imageView;
    }
}
