package au.edu.holmesglen.victoriav.threeinarow;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

/**
 * MenuActivity Class
 * @author Victoria Vilarroel
 * Description: Dsiplays the menu and starts the activity selected by the user (game, settings, help,
 * high scores)
 */
public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        Button btnStart=findViewById(R.id.btnStart);
        Button btnSettings=findViewById(R.id.btnSettings);
        Button btnHigh=findViewById(R.id.btnHighScores);

        FloatingActionButton btnHelp = findViewById(R.id.btnHelp);

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getApplicationContext(),GameActivity.class);
                startActivity(i);
            }
        });
        btnSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getApplicationContext(),SettingsActivity.class);
                startActivity(i);
            }
        });
        btnHigh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getApplicationContext(),HighScoresActivity.class);
                startActivity(i);
            }
        });
        btnHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getApplicationContext(),HelpActivity.class);
                startActivity(i);
            }
        });
    }



}
