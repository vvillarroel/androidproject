package au.edu.holmesglen.victoriav.threeinarow;


import android.support.v7.app.AppCompatActivity;

/**
 * Item Class
 * @author Victoria Vilarroel
 * Description: Contains the Item object,constructors and other methods
 */
public class Item extends AppCompatActivity {
    private int img;
    private String title;

    public Item() {
        this.img = 0;
        this.title = "";
    }

    public Item(int colImg, String title) {
        this.img = colImg;
        this.title = title;
    }

    public void setItem(Item i) {
        this.img = i.img;
        this.title = i.title;
    }

    public Item getItem() {
        return this;
    }

    public int getColor() {
        return this.img;
    }

    public void setColor(int position) {
        img = position;
    }

    public String getItemTitle() {
        return this.title;
    }

    public static boolean compareItem(Item color1, Item color2) {
        if (color1.getColor() == color2.getColor()) {
            return false;
        } else {
            return true;
        }
    }

}
