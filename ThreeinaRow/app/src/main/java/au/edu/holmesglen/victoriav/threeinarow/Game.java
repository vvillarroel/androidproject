package au.edu.holmesglen.victoriav.threeinarow;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;

/**
 * Game Class
 * @author Victoria Vilarroel
 * Description: Contains the game object and the game logic for the application.
 */
public class Game {
    //items
    final private Item baseColor = new Item(R.drawable.grey, "Grey");
    //playable colors
    private Item color1 = new Item();
    private Item color2 = new Item();

    private Item[] gridArray;

    private Item[] color; //current color
    private Item last; //last color used
    //int
    private int[] randoms;
    private int[] allGrid;
    private int numItems;
    private int gridSize;
    //others
    private long time; //FUTURE FEATURE
    private Context gameContext;
    private ImageView hint;

    /**
     * Game Constructor
     * Description: Initialize the game setting the grid size, the colours, the nextColor.a
     * @param size
     * @param fColor
     * @param sColor
     * @param grid
     * @param hint
     * @param context
     */
    public Game(int size, Item fColor, Item sColor, GridView grid, ImageView hint, Context context) {

        gridSize = size;
        this.numItems = gridSize * gridSize;
        gameContext = context;

        color1.setItem(fColor.getItem());
        color2.setItem(sColor.getItem());

        generateRandom();
        allGrid = new int[numItems];
        addRandom();

        assignInitColors();

        last = new Item();
        last.setItem(baseColor.getItem());

        color = new Item[1];
        color[0] = new Item();
        color[0].setItem(alternateColor());
        hint.setImageResource(color[0].getColor());
        grid.setAdapter(new ImageAdapter(gameContext, gridArray));
        this.hint=hint;
    }


    /**
     *  generateRandom Method
     *
     */
    public void generateRandom() {
        randoms = new int[numItems];
        boolean flag = false;
        int random = 0;
        for (int i = 0; i < 4; i++) {
            do {
                random = (int) (Math.random() * (numItems - 1)) + 1;
                flag = checkRandomArray(random);
            } while (!flag);
            randoms[i] = random;
            Log.i("generateRandom", "random: " + random);
        }
        Log.i("generateRandom", "randoms: " + randoms.toString());
    }

    /**
     *
     * @param value
     * @return
     */
    public boolean checkRandomArray(int value) {
        if (randoms.length == 0) {
            Log.i("checkRandomArray", "Empty Array");
            return true;
        }
        for (int valueInArray : randoms) {
            Log.i("checkRandomArray", "Value " + valueInArray);

            if (valueInArray == value) {
                Log.i("checkRandomArray", "Value Exists" + valueInArray);
                return false;
            }
        }
        Log.i("checkRandomArray", "Array filled");
        return true;
    }

    /**
     *
     */
    public void assignInitColors() {
        gridArray = new Item[numItems];

        for (int i = 0; i < numItems; i++) {
            gridArray[i] = baseColor;
        }
        for (int i = 0; i < 2; i++) {
            Log.i("colorAssigned", "Color 1 at " + randoms[i]);
            gridArray[randoms[i] - 1] = (color1);
        }
        for (int i = 2; i < 4; i++) {
            Log.i("colorAssigned", "Color 2 at " + randoms[i]);
            gridArray[randoms[i] - 1] = (color2);
        }

    }

    /**
     *
     */
    public void addRandom() {
        for (int i = 0; i < randoms.length; i++) {
            allGrid[i] = randoms[i];
            Log.i("addRandom", "Cannot be clicked init " + randoms[i]);
        }
    }

    /**
     *
     * @return
     */
    public Item alternateColor() {
        Item i = new Item();
        Log.i("alternateColor", "last color: " + last.getItem().getItemTitle());
        if (Item.compareItem(color1, last) && Item.compareItem(color2, last)) {
            int random = (int) (Math.random() * 200 + 1);
            Log.i("alternateColor", "-random number: " + random);
            if (random % 2 == 0) {
                i.setItem(color1.getItem());
                Log.i("alternateColor", "--selected Color 1: " + i.getItemTitle());
            } else {
                i.setItem(color2.getItem());
                Log.i("alternateColor", "--selected Color 2: " + i.getItemTitle());
            }
        } else {
            if (!Item.compareItem(last, color1)) {
                i.setItem(color2.getItem());
                Log.i("alternateColor", "--selected Color 1: " + i.getItemTitle());
            } else {
                i.setItem(color1.getItem());
                Log.i("alternateColor", "--selected Color 2: " + i.getItemTitle());
            }
        }
        last.setItem(i.getItem());
        Log.i("alternateColor", "-new last color: " + i.getItemTitle());

        return i;
    }

    /**
     *
     * @param position
     * @return
     */
    public boolean positionChecking(int position) {
        for (int random : allGrid) {
            if (random == position + 1) {
                Log.i("randomChecking", "Cannot be clicked " + random);
                return false;
            }
        }
        Log.i("randomChecking", "can be clicked ");
        addMove(position);
        return true;
    }

    /**
     *
     * @param position
     */
    public void addMove(int position) {
        for (int i = 0; i < allGrid.length; i++) {
            Log.i("addClicked", "val " + allGrid[i]);
            if (allGrid[i] == 0) {
                allGrid[i] = position + 1;
                Log.i("addClicked", "-added " + position);
                break;
            }
        }
    }

    /**
     *
     * @return
     */
    public boolean endOfMoves() {
        for (int i = 0; i < numItems; i++) {
            if (allGrid[i] == 0) {
                return false;
            }
        }
        return true;
    }

    /**
     *
     * @return
     */
    public boolean checkGame() {
        //Horizontal
        Log.i("items", "checking 3 in row horizontally");
        Log.i("items", "-----------------------------------------------------------------");
        for (int i = 0; i < numItems; i += gridSize) {
            if (checkColor(color1, i)) {
                Log.i("items", "3 in row horizontally --------------------------------");
                return true;
            } else if (checkColor(color2, i)) {
                Log.i("items", "3 in row horizontally --------------------------------");
                return true;
            }
        }
        //vertical
        Log.i("items", "checking 3 in row vertically");
        Log.i("items", "-----------------------------------------------------------------");
        for (int i = 0; i < gridSize * 2; i++) {
            Item item0 = gridArray[i].getItem();
            Item item1 = gridArray[i + (gridSize)].getItem();
            Item item2 = gridArray[i + 2 * (gridSize)].getItem();
            Log.i("items", "ITEM 0 " + item0.getItemTitle() + "- ITEM 1 " + item1.getItemTitle() + "- ITEM 2 " + item2.getItemTitle());
            if (Item.compareItem(item0, baseColor) && !Item.compareItem(item0, item1) && !Item.compareItem(item1, item2)) {
                Log.i("items", "3 in row vertically --------------------------------");
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @param color
     * @param i
     * @return
     */
    public boolean checkColor(Item color, int i) {
        Item item0 = gridArray[i].getItem();
        Item item1 = gridArray[i + 1].getItem();
        Item item2 = gridArray[i + 2].getItem();
        Item item3 = gridArray[i + 3].getItem();
        Log.i("items", "ITEM 0 " + item0.getItemTitle() + "- ITEM 1 " + item1.getItemTitle() + "- ITEM 2 " + item2.getItemTitle() + "- ITEM 3 " + item3.getItemTitle());

        if ((!Item.compareItem(item0, color) && !Item.compareItem(item1, color) && !Item.compareItem(item2, color))
                || (!Item.compareItem(item1, color) && !Item.compareItem(item2, color) && !Item.compareItem(item3, color))) {
            return true;
        }
        return false;
    }

    /**
     *
     * @param position
     * @param view
     */
    public void move(int position, View view) {

        if (positionChecking(position)) {
            Log.i("MOVE", "COLOR[0]=" +color[0].getItemTitle() );
            gridArray[position] = color[0];
            color[0] = alternateColor();
            hint.setImageResource(color[0].getColor());
            int c = gridArray[position].getColor();
            ((ImageView) view).setImageResource(c);
            Log.i("MOVE", "COLOR[0]=" +color[0].getItemTitle() );
        }
    }
}
